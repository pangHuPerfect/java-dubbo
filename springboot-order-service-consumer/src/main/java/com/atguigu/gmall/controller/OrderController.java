package com.atguigu.gmall.controller;

import com.atguigu.gmall.bean.UserAddress;

import com.atguigu.gmall.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(tags = "消费者服务的http接口")
@RestController
@RequestMapping("/")
public class OrderController {
    @Resource
    OrderService orderService;

    @ApiOperation("初始化订单接口")
    @ApiParam("uid代表用户id")
    @GetMapping("/initOrder")
    public List<UserAddress> initOrder(@RequestParam("uid") String userId){
        return orderService.initOrder(userId);
    }

    @ApiOperation("根据指定城市, 获取特定的用户地址接口")
    @GetMapping("/getAreaAddressList")
    public List<UserAddress> getAreaAddressList(@RequestParam("address") String addr){
        Map<String, String> addressMap = new HashMap<>();
        addressMap.put("address", addr);
        return orderService.getAreaAddressList(addressMap);
    }
    @ApiOperation("获取总用户数")
    @GetMapping("/getTotalUser")
    public Integer getTotalUser(){
        return orderService.getTotalUser();
    }

    @ApiOperation("根据用户id获取这些用户的地址接口(目前是固定的, 只会返回userid分别为1,2,3的数据)")
    @PostMapping("/getUserList")
    public Map<String, List<String>> getUserList(@RequestBody Map<String, List<String>> userInfoMap){
        //{"userIdList": ["1","2","3"]}
        List userIdList = userInfoMap.get("userIdList");
        System.out.println("userIdList: " + userIdList);
        return orderService.getUserList(userIdList);
    }


}
