package com.atguigu.gmall.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.atguigu.gmall.bean.UserAddress;
import com.atguigu.gmall.service.OrderService;
import com.atguigu.gmall.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/*
* 1. 将服务提供者注册到注册中心
*       - 导入 dubbo 2.6.2 版本依赖，zookeeper依赖
*       - 配置服务提供者
*
*
* 2. 让服务消费者去注册中心订阅服务提供者的服务地址
*
* */
@Service
public class OrderServiceImpl implements OrderService {
    @Reference
    UserService userService;


    @Override
    public List<UserAddress> initOrder(String userId) {
        return userService.getOneUserAddressList(userId);
    }

    @Override
    public List<UserAddress> getAreaAddressList(Map<String, String> addressMap) {

        return userService.getSpecificAddressList(addressMap);
    }

    @Override
    public Integer getTotalUser() {
        return userService.getUserCount();
    }

    @Override
    public Map<String, List<String>> getUserList(List<String> userIdList) {
        return userService.getUserAddressList(userIdList);
    }
}
