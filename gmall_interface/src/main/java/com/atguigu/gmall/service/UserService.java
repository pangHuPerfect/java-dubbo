package com.atguigu.gmall.service;

import com.atguigu.gmall.bean.UserAddress;

import java.util.List;
import java.util.Map;


public interface UserService {
    //按照用户id返回该用户所有的收货地址
	public List<UserAddress> getOneUserAddressList(String userId);

	//根据传入的{"省份": "城市"},获取在指定城市的用户列表
	public List<UserAddress> getSpecificAddressList(Map<String, String> addressMap);

	//统计当前共有多少用户
	public Integer getUserCount();

	//根据传入的userId列表, 输出每个userId对应的地址信息, 以hashmap形式返回
	public Map<String, List<String>> getUserAddressList(List<String> userIdList);
}
