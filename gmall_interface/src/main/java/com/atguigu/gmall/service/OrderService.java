package com.atguigu.gmall.service;

import com.atguigu.gmall.bean.UserAddress;

import java.util.List;
import java.util.Map;

public interface OrderService {
	
	/**
	 * 初始化订单
	 * @param userId
	 */
	public List<UserAddress> initOrder(String userId);

	public List<UserAddress> getAreaAddressList(Map<String, String> addressMap);

	public Integer getTotalUser();

	public Map<String, List<String>> getUserList(List<String> userIdList);

}
