package com.atguigu.gmall.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.atguigu.gmall.bean.UserAddress;
import com.atguigu.gmall.service.UserService;
import org.springframework.stereotype.Component;

import java.util.*;

@Service  //dubbo家的service注解, 标明要暴露的服务!
@Component
public class UserServiceImpl implements UserService {
    public List<UserAddress> getOneUserAddressList(String userId) {
	    //假设下面是根据传入的userId从数据库中查到的2条数据
        UserAddress address1 = new UserAddress(1, "广东省珠海市金湾区XXX大厦A座", userId, "Eric", "15944175719", "Y");
        UserAddress address2 = new UserAddress(2, "广东省珠海市金湾区XXXX小区", userId, "Eric", "15944175719", "N");
        return Arrays.asList(address1, address2);
    }

    public List<UserAddress> getSpecificAddressList(Map<String, String> addressMap) {
        //{"address":"广东省深圳市"}
        String addrInput = addressMap.get("address");
        UserAddress address1 = new UserAddress(300, addrInput + "XXX大厦F座", "130", "Lucy", "15991923491", "Y");
        UserAddress address2 = new UserAddress(305, addrInput + "XXX景区", "160", "Tony", "13660415233", "Y");
        return Arrays.asList(address1, address2);
    }

    public Integer getUserCount() {
        return 100000;
    }

    public Map<String, List<String>> getUserAddressList(List<String> userIdList) {
        //模拟传入的数据为 ["10","11","12"]
        //假设下面是根据传入的userId从数据库中查到的数据, 固定返回3个userId的数据(每个userId2条)
        UserAddress address1 = new UserAddress(100, "广东省深圳市南山区XXX大厦B座", "10", "Smith", "13689045713", "Y");
        UserAddress address2 = new UserAddress(101, "广东省深圳市南山区XXX街道10号楼", "10", "Smith", "13689045713", "N");
        UserAddress address3 = new UserAddress(120, "北京市海淀区XXX小区", "11", "Martin", "18697348400", "N");
        UserAddress address4 = new UserAddress(121, "北京市东城区XXX大厦C座", "11", "Martin", "18697348400", "Y");
        UserAddress address5 = new UserAddress(125, "山东省威海市环翠区XXX旅游景点", "12", "Jackson", "15071943763", "N");
        UserAddress address6 = new UserAddress(126, "山东省威海市文登区XXX办公楼", "12", "Jackson", "15071943763", "Y");
        Map<String, List<String>> userAddressMap = new HashMap<>();

        List<String> arr1 = new ArrayList<>();
        List<String> arr2 = new ArrayList<>();
        List<String> arr3 = new ArrayList<>();
        arr1.add(address1.getUserAddress());
        arr1.add(address2.getUserAddress());

        arr2.add(address3.getUserAddress());
        arr2.add(address4.getUserAddress());

        arr3.add(address5.getUserAddress());
        arr3.add(address6.getUserAddress());

        userAddressMap.put("10", arr1);
        userAddressMap.put("11", arr2);
        userAddressMap.put("12", arr3);
        return userAddressMap;
    }
}
